import ReactCompareImage from 'react-compare-image';
import './App.css';
import color_difference_form_back from './Images/color_difference_form_back.jpg';
import color_difference_form_front from './Images/color_difference_form_front.jpg';
import information_loss_map_A from './Images/information_loss_map_A.jpg';
import information_loss_map_B from './Images/information_loss_map_B.jpg';
import stripes_ship_blueprint_A from './Images/stripes_ship_blueprint_A.jpg';
import stripes_ship_blueprint_B from './Images/stripes_ship_blueprint_B.jpg';
import stripes_ship_side_profile_A from './Images/stripes_ship_side_profile_A.jpg';
import stripes_ship_side_profile_B from './Images/stripes_ship_side_profile_B.jpg';

function App() {
    return (
        <div className="App">
            <h1>Image comparison</h1>

            <hr></hr>

            <h2>Information loss example</h2>
            <ReactCompareImage
                leftImage={information_loss_map_A}
                leftImageAlt="A map of an area with clearly drawn houses, lots, and countour lines."
                rightImage={information_loss_map_B}
                rightImageAlt="A map of an area with clearly drawn houses and lots, but is missing contour lines."
            />

            <h2>Unwanted artifacts examples</h2>
            <ReactCompareImage
                leftImage={stripes_ship_blueprint_A}
                leftImageAlt="A blueprint of a ship."
                rightImage={stripes_ship_blueprint_B}
                rightImageAlt="A blueprint of a ship, but the image has unwanted vertical lines on it."
            />
            <ReactCompareImage
                leftImage={stripes_ship_side_profile_A}
                leftImageAlt="A drawing of a the side profile of a ship."
                rightImage={stripes_ship_side_profile_B}
                rightImageAlt="A drawing of a the side profile of a ship, but the image has unwanted vertical lines on it."
            />

            <h2>Difference in scanners example</h2>
            <p>
                These images are from the same scanner that scans both sides at once, front and back.
                The front page is digitized correctly, but the back page shows a blue tone.
            </p>
            <div className="sideBySide">
                <img src={color_difference_form_front} alt="An A4 document of a form." />
                <img src={color_difference_form_back} alt="An A4 document of a form, but the paper has a blue tone to it instead of white." />
            </div>
        </div>
    );
}

export default App;
